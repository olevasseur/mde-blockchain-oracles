const express = require('express');
const offChainApp = express();
const https = require('https');
const fs = require('fs');
let numberOfTickets = 0;

const getSSLOptionsParameters = () => {
	return {
        key: fs.readFileSync(`./certs/offchain_key.pem`),
        cert: fs.readFileSync(`./certs/offchain_certificate.pem`),
        ca: [
          fs.readFileSync(`./certs/ca_certificate.pem`)
          
        ],
	};
}

offChainApp.use(express.json({ type: '*/*' }));

offChainApp.use('/setNumberOfTickets', (req, res, next) => {
    numberOfTickets = req.body.number;
    console.log('setting new number of tickets to buy: ' + numberOfTickets);
    const data = JSON.stringify({
        newNumber: numberOfTickets,
    });
    res.write(data)
    res.end();
});

offChainApp.use('/getNumberOfTickets', (req, res, next) => {
    console.log('received request for number of tickets to buy');
    const json = {
        numberOfTickets: numberOfTickets,
        value: numberOfTickets * 0.01,
    };
    // we want to buy that number of tickets only once
    if (numberOfTickets > 0) {
        numberOfTickets = 0;
        json.message = "Number of tickets will be reinitialized to 0 now.";
    }
    const data = JSON.stringify(json);
    res.write(data);
    res.end();
});

offChainApp.use('/winnerDeclared', (req, res, next) => {
    console.log('Winner has been declared: ' + req.body.winner);
    res.end();
});

offChainApp.use('/requestExchangeRate', (req, res, next) => {
    console.log('receive exchange rate request');
    const data = JSON.stringify({
        exchangeRate: 310000000000000
    });
    res.write(data);
    res.end();
});

offChainApp.use('/transactionConfirmation', (req, res) => {
    console.log('received transaction confirmation: ' + req.body.transactionHash + ' success: ' + req.body.success);
    res.end();
});

const options = {
      // Requesting the client to provide a certificate, to authenticate.
      requestCert: true,
      // As specified as "true", so no unauthenticated traffic
      // will make it to the specified route specified
      rejectUnauthorized: true
    };
const offChain = https.createServer(Object.assign(options, getSSLOptionsParameters()), offChainApp);
// const offChain = http.createServer(offChainApp);
offChain.listen(3001);
