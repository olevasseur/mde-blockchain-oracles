// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

contract Lottery {
    // owner of the lottery
    address public owner;
    // price of one ticket in Wei
    uint ticketPrice;
    // total number of tickets available in the lottery
    uint remainingTickets;
    // maximum number of tickets that a customer can buy
    uint ticketLimitPerCustomer;
    // lottery prize in USD
    uint lotteryPrize;
    // exchange rate: number of ETH for 1 USD
    uint rate;

    struct Customer {
        // number of tickets the customer has bought
        uint numberOfTickets;
        // index of the customer in customerAddresses
        // used to verify if customer exists (see customerExists) 
        uint id;
    }
    // mapping between address and Customer struct
    mapping(address => Customer) customers;
    // for each customer who has bought tickets, this array contains
    // one entry with the customer's address
    address [] customerAddresses;
    // for each ticket bough by a customer, this array contains one
    // entry with the customer's address
    address [] tickets;

    // Allow customers to buy tickets simply by sending money
    receive () external payable {
        buyTickets();
    }

    function buyTickets() public payable {
        // customer must buy at least one ticket
        require(msg.value > 0, 'msg.value must at least cover 1 ticket');
        // customer cannot buy a fraction of a ticket, so amount sent must be
        // a multiple of ticketPrice
        require((msg.value % ticketPrice) == 0, 'msg.value must be a multiple of ticket price');

        uint numberOfTickets = uint(msg.value)/uint(ticketPrice);

        // if ticketLimitPerCustomer is 0, then there is no limit
        if (ticketLimitPerCustomer > 0) {
            uint customerNumberOfTickets = getCustomerNumberOfTickets(msg.sender);
            require(customerNumberOfTickets + numberOfTickets < ticketLimitPerCustomer, 'Trying to buy too many tickets');
        }
        // customer cannot buy more than the number of remaining tickets
        require(numberOfTickets <= remainingTickets, 'Cannot buy more than remaining number of tickets');

        if (!customerExists(msg.sender)) {
            // if customer does not exist, add customer to customerAddresses and
            // set the customer's id
            customers[msg.sender].id = customerAddresses.length;
            customerAddresses.push(msg.sender);

            // initialize customer's number of tickets
            customers[msg.sender].numberOfTickets = numberOfTickets;
        } else {
            // increment customer's number of tickets
            customers[msg.sender].numberOfTickets = customers[msg.sender].numberOfTickets + numberOfTickets;
        }

        // add one entry to tickets array for each ticket bought by customer
        for (uint counter=0; counter < numberOfTickets; counter++) {
            tickets.push(msg.sender);
        }
        // update remainingTickets
        remainingTickets = remainingTickets - numberOfTickets;
        if (remainingTickets == 0) {
            emit RequestForExchangeRate();
        }
    }


    function getNumberOfRemainingTickets() public view returns(uint) {
        return remainingTickets;
    }

    constructor(
        uint _totalNumberOfTickets, 
        uint _ticketPrice, 
        uint _ticketLimitPerCustomer,
        uint _lotteryPrize
    ) {
        owner = msg.sender;
        remainingTickets = _totalNumberOfTickets;
        ticketPrice = _ticketPrice;
        ticketLimitPerCustomer = _ticketLimitPerCustomer;
        lotteryPrize = _lotteryPrize;
        rate = 0;
    }
    function getNumberOfCustomers() public view returns(uint) {
        return customerAddresses.length;
    }

    function customerExists(address customerAddress) private view returns(bool) {
        uint index = customers[customerAddress].id;
        // in case customers mapping contains old data after resetting lottery, we want to avoid
        // accessing the customerAddresses at at index that is out of bounds
        if (index + 1 > customerAddresses.length) {
            return false;
        } else {
            // make sure that the address of the customer is present in the customerAddresses array
            return customerAddresses[index] == customerAddress;
        }
        
    }

    function getCustomerNumberOfTickets(address customerAddress) public view returns(uint) {
        if (customerExists(customerAddress)) {
            return customers[customerAddress].numberOfTickets;
        } else {
            return 0;
        }
    }

    // exchangeRate is the number of wei for 1 USD
    function updateExchangeRate(uint exchangeRate) public ownerOnly {
        require(remainingTickets == 0, 'Cannot update exchange rate before lottery is over');
        rate = exchangeRate;
        closeLottery(exchangeRate);
    }

    function getBalance() public view ownerOnly returns(uint) {
        return address(this).balance;
    }

    function getExchangeRate() public view returns(uint) {
        return rate;
    }

    function resetLottery(
        uint _totalNumberOfTickets, 
        uint _ticketPrice, 
        uint _ticketLimitPerCustomer,
        uint _lotteryPrize
    ) public ownerOnly {
        remainingTickets = _totalNumberOfTickets;
        ticketPrice = _ticketPrice;
        ticketLimitPerCustomer = _ticketLimitPerCustomer;
        lotteryPrize = _lotteryPrize;
        // reset customerAddresses
        delete customerAddresses;
        // reset tickets
        delete tickets;
        // no need to reset elements in customers mapping, because since customerAddresses
        // has been reset, any player will always be considered a new player
        rate = 0;
    } 

    function closeLottery(uint exchangeRate) private {
        address payable winner = payable(getWinner());
        winner.transfer(exchangeRate * lotteryPrize);
        emit AnnounceWinner(winner);
    }

    function getWinner() private view returns(address) {
        // not a true random number
        uint randomNumber = uint(keccak256(
            abi.encodePacked(
                block.timestamp, 
                msg.sender, 
                tickets.length)
            )
        );
        uint winnerIndex = randomNumber % tickets.length;
        return tickets[winnerIndex];
    }

    modifier ownerOnly() {
        require(msg.sender == owner, 'only owner is allowed to call this function');
        _;
    }

    event RequestForExchangeRate();
    event AnnounceWinner(address winner);
}
    
