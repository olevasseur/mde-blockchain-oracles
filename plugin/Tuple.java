package oracleplugin;

public class Tuple {
    public String name;
    public Object value;
    public Tuple (String name, Object value) {
        this.name = name;
        this.value = value;
    }
}
