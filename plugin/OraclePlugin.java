package oracleplugin;
import com.nomagic.magicdraw.plugins.Plugin;
import javax.swing.*;

/**
Les imports plus bas ont été copiés du fichier ActionsExamplePlugin.java
*/
import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.ActionsID;
import com.nomagic.magicdraw.actions.MDAction;
import com.nomagic.magicdraw.plugins.Plugin;
import com.nomagic.magicdraw.ui.actions.DefaultDiagramAction;
import com.nomagic.magicdraw.ui.browser.actions.DefaultBrowserAction;
import com.nomagic.magicdraw.uml.DiagramType;


public class OraclePlugin extends Plugin
{
	public static boolean initialized;
	@Override
	public void init()
	{
		initialized = true;
		final ActionsConfiguratorsManager manager = ActionsConfiguratorsManager.getInstance();
		final MDAction action = new GenerateCodeAction();
		manager.addMainToolbarConfigurator(new MainToolbarConfigurator(action));
		JOptionPane.showMessageDialog(null, "Plugin successfully opened");

	}
	@Override
	public boolean close()
	{
		JOptionPane.showMessageDialog( null, "Plugin successfully closed");
		return true;
	}
	@Override
	public boolean isSupported()
	{
		//plugin can check here for specific conditions
		//if false is returned plugin is not loaded.
		return true;
	}
}