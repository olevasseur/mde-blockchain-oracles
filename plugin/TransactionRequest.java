package oracleplugin;

public class TransactionRequest extends OracleInteraction implements VerifiableElement {
    private OffChainApplication source;
    private Bridge destination;

    @Override
    public Object getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, Bridge.class);
        this.destination = (Bridge)destination;
    }

    @Override
    public Object getSource() {
        return this.source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, OffChainApplication.class);
        this.source = (OffChainApplication)source;
    }

    @Override
    public void checkForError() {

    }
}
