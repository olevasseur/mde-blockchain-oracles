package oracleplugin;

public class EventNotification extends OracleInteraction implements VerifiableElement {
    private OffChainApplication receiver;
    private Bridge source;
    private String notificationPath;
    public Object getDestination() {
        return receiver;
    }
    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, OffChainApplication.class);
        this.receiver = (OffChainApplication) destination;
    }

    @Override
    public Object getSource() {
        return source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, Bridge.class);
        this.source = (Bridge)source;
    }

    public String getNotificationPath() {
        return notificationPath;
    }

    public EventNotification() {

    }

    @Override
    public void checkForError() throws InvalidModelException {
        if (this.notificationPath == null || this.notificationPath.equals("")) {
            throw new InvalidModelException("EventNotification must have a notificationPath");
        }
    }
}
