package oracleplugin;

import java.util.ArrayList;
import java.util.List;

public class OffChainEventSubscription implements VerifiableElement {
    OffChainApplication dataSource = null;
    OffChainQuery offChainQuery = null;
    List<OracleConstraint> constraints = new ArrayList<>();
    SendTransaction associatedTransaction;
    int queryInterval;

    public String getId() {
        String host = dataSource.getHost();
        int port = dataSource.getPort();
        String path = offChainQuery.getQueryPath();
        return host + port + path;
    }

    public int getQueryInterval() {
        return queryInterval;
    }

    public void setQueryInterval(int queryInterval) {
        this.queryInterval = queryInterval;
    }

    public OffChainQuery getOffChainQuery() {
        return offChainQuery;
    }

    public void setOffChainQuery(OffChainQuery offChainQuery) {
        this.offChainQuery = offChainQuery;
    }

    public SendTransaction getAssociatedTransaction() {
        return associatedTransaction;
    }

    public void setAssociatedTransaction(SendTransaction associatedTransaction) {
        this.associatedTransaction = associatedTransaction;
    }

    public OffChainApplication getDataSource() {
        return dataSource;
    }

    public void setDataSource(OffChainApplication dataSource) {
        this.dataSource = dataSource;
    }
    public void addConstraint(OracleConstraint constraint) {
        this.constraints.add(constraint);
    }
    public List<OracleConstraint> getConstraints() {return this.constraints;}

    @Override
    public void checkForError() throws InvalidModelException {
        this.getAssociatedTransaction().checkForError();
    }
}
