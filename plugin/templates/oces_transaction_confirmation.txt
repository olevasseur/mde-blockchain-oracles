.on('confirmation', (confirmationNumber, receipt) => {
						let transactionHash = receipt.transactionHash;
						console.log('transaction confirmation number ' + confirmationNumber);
						if (attributes.confirmationPath) {
							const hostname = attributes.confirmationPath.split("/")[0].split(":")[0];
							const port = attributes.confirmationPath.split("/")[0].split(":")[1];
							const path = "/" + attributes.confirmationPath.split("/")[1];
							if (confirmationNumber == attributes.numberOfConfirmations) {
								web3.eth.getTransactionReceipt(transactionHash, (e, receipt) => {
									let data;
									if (e || receipt == null) {
										data = JSON.stringify({
											'transactionHash': transactionHash,
											success: false
										});
									} else {
										data = JSON.stringify({
											'transactionHash': transactionHash,
											success: true
										});

									}
									const options = getPostRequestOptions(hostname, path, port, data);
									let request = https.request(options);
									request.on('error', (error) => {
										console.log('could not communicate with ' + attributes.confirmationPath + " : " + error);
									});
									request.write(data);
									request.end();
									return;
								});
							}
						}
					}).on('error', (error) => {
						console.log('error while trying to get transaction confirmation: ' + error);
					});
