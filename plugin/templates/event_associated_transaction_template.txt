, async (res) => {
			const body = [];

			res.on('data', d => {
				body.push(d);
			});
			res.on('end', async () => {
				let requestBody;
				try {
					const parsedBody = Buffer.concat(body).toString();
					requestBody = JSON.parse(parsedBody);
				} catch (error) {
					console.log(error);
					return;
				}

				// transaction attributes
				%1$s
				// transaction parameters
				%2$s
				overwriteModelTransactionParameters(attributes.contractAddress, attributes.functionName, parameters, requestBody);
				overwriteModelTransactionAttributes(attributes, requestBody);
				let data = getTransactionDataFromDict(parameters, attributes.contractAddress, attributes.functionName);
				// if data could not be properly generated
				if (data !== undefined && !data.startsWith('0x')) {
					console.log(data);
					return;
				}
				const details = await getTransactionDetails(data, attributes);
				const chainName = getChainName(attributes.chainId);
				const transaction = new EthereumTx(details, {chain:chainName});
				const privateKey = attributes['privateKey'];
				transaction.sign(Buffer.from(privateKey, 'hex'));
				const serializedTransaction = transaction.serialize()
				console.log('sending transaction ' + transaction.hash(true).toString('hex'));
				let transactionHash;
				await web3.eth.sendSignedTransaction('0x' + serializedTransaction.toString('hex'))
				.on('receipt', (receipt) => {
					transactionHash = receipt.transactionHash;
//					return res.status(200).json({status:200, transactionHash: receipt.transactionHash}).end();
				}).on('confirmation', (confirmationNumber, receipt) => {
					console.log('transaction confirmation number ' + confirmationNumber);
					if (attributes.confirmationPath) {
						const hostname = attributes.confirmationPath.split("/")[0].split(":")[0];
						const port = attributes.confirmationPath.split("/")[0].split(":")[1];
						const path = "/" + attributes.confirmationPath.split("/")[1];
						if (confirmationNumber == attributes.numberOfConfirmations) {
							web3.eth.getTransactionReceipt(transactionHash, (e, receipt) => {
								let data;
								if (e || receipt == null) {
									data = JSON.stringify({
										'transactionHash': transactionHash,
										success: false
									});
								} else {
									data = JSON.stringify({
										'transactionHash': transactionHash,
										success: true
									});

								}
								const options = getPostRequestOptions(hostname, path, port, data);
								let request = https.request(options);
								request.on('error', (error) => {
									console.log('could not communicate with ' + attributes.confirmationPath + " : " + error);
								});
								request.write(data);
								request.end();
								return;
							});

						}
					}
				}).on('error', (error) => {
					console.log(error);
				});
			});
		}