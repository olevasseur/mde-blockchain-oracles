package oracleplugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShellCommand {
    private static final String RSA_CERTIFICATE_TYPE = "rsaEncryption";
    private static final String EC_CERTIFICATE_TYPE = "id-ecPublicKey";
    private static final String WINDOWS_GREP = "findstr /C:";
    private static final String UNIX_GREP = "grep";
    private static final String GET_CERTIFICATE_KEY_TYPE = "openssl x509 -in %1$s -text | %2$s\'Public Key Algorithm\'";
    private static final String GET_CERTIFICATE_KEY_SIZE = "openssl x509 -in %1$s -text | %2$s\'Public-Key\'";
    private static final String GET_PUB_FROM_CERT = "openssl x509 -pubkey -noout -in %1$s";
    private static final String GET_PUB_FROM_PRIV = "openssl %1$s -in %2$s -pubout";
    private static final Set checkedCertificates = new HashSet<>();
    private static boolean isWindows() {
        return  System.getProperty("os.name")
                .toLowerCase().startsWith("windows");
    }

    private static Process getProcess(String commands) {
        String homeDirectory = System.getProperty("user.home");
        Process process;
        try {
            process = Runtime.getRuntime().exec(commands);
        } catch (java.io.IOException  e) {
            return null;
        }
        return process;
    }
    public static boolean openSSLInstalled() {
        Process process = getProcess("openssl version");
        return process.exitValue() == 0;
    }
    private static boolean isValidCertificate(String certificatePath) {
        String [] commands = {"openssl", "x509", "-in", certificatePath, "-text"};
        String command = "openssl x509 -in " + certificatePath + " -text";
        Process process = getProcess(command);
        String output = getCommandOutput(process);
        return process.exitValue() == 0;
    }
    private static String getCertificateKeyType(String certificatePath) throws InvalidModelException {
        if (!isValidCertificate(certificatePath)) {
            throw new InvalidModelException("Invalid certificate file");
        }
        boolean isWindows = isWindows();
        String command = "";
        if (isWindows) {
            command = String.format(GET_CERTIFICATE_KEY_TYPE, certificatePath, WINDOWS_GREP);
            command = "powershell " + command;
        } else {
            // on windows, the search string must immediately follow "findstr /C:"
            // on unix, a whitespace needs to be added after grep
            command = String.format(GET_CERTIFICATE_KEY_TYPE, certificatePath, UNIX_GREP + " ");
            command = "bash -c " + command;
        }
        Process process = getProcess(command);
        String output = getCommandOutput(process);
        if (output.contains(RSA_CERTIFICATE_TYPE)) {
            return RSA_CERTIFICATE_TYPE;
        } else if (output.contains(EC_CERTIFICATE_TYPE)) {
            return EC_CERTIFICATE_TYPE;
        } else {
            throw new InvalidModelException("Unsupported certificate type");
        }
    }
    private static String getMD5(String path) throws InvalidModelException {
        try (InputStream is = Files.newInputStream(Paths.get(path))) {
            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
            return md5;
        } catch (IOException e) {
            throw new InvalidModelException(e.toString());
        }
    }
    private static void checkCertificateKeySize(String certificatePath, String keyType) throws InvalidModelException {
        String command;
        if (isWindows()) {
            command = String.format(GET_CERTIFICATE_KEY_SIZE, certificatePath, WINDOWS_GREP);
            command = "powershell " + command;
        } else {
            command = String.format(GET_CERTIFICATE_KEY_SIZE, certificatePath, WINDOWS_GREP);
            command = "bash -c " + command;
        }
        Process process = getProcess(command);
        String output = getCommandOutput(process);
        String sizeString = output.replaceAll("[^\\d.]", "");
        int size = Integer.valueOf(sizeString);
        if (keyType.equals(RSA_CERTIFICATE_TYPE)) {
            if (size < 3072) {
                throw new InvalidModelException("RSA keys should have a minimum size of 3072");
            }
        } else {
            if (size < 256) {
                throw new InvalidModelException("ECDSA keys should have a minimum size of 256");
            }
        }
    }
    public static String checkCertificateAndAssociatedKey(String certificatePath, String privateKeyPath) throws InvalidModelException {
        String md5Cert = getMD5(certificatePath);
        String md5Key = getMD5(privateKeyPath);
        String hashKey = md5Cert+md5Key;
        if (!checkedCertificates.contains(hashKey)) {
            String certificateKeyType = getCertificateKeyType(certificatePath);
            checkCertificateKeySize(certificatePath, certificateKeyType);
            String publicKey = getPublicKeyFromPrivateKey(privateKeyPath, certificateKeyType);
            String publicKeyCert = getPublicKeyFromCertificate(certificatePath);
            if (!publicKey.equals(publicKeyCert)) {
                throw new InvalidModelException("Private key file does not match public key in certificate");
            }
            checkedCertificates.add(hashKey);

            if (certificateKeyType.equals(EC_CERTIFICATE_TYPE)) {

            } else {

            }
        }

        return null;
    }
    private static String getKeyFromOutput(String output) {
        String patternStr = "-----[a-zA-Z ]+-----";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(output);
        matcher.find();
        int startKey = matcher.end();
        matcher.find();
        int endKey = matcher.start();
        String pubKey = output.substring(startKey, endKey);
        return pubKey;
    }
    private static String getPublicKeyFromCertificate(String certificatePath) {
        String command = String.format("openssl x509 -in %1$s -pubkey -noout", certificatePath);
        Process process = getProcess(command);
        String output = getCommandOutput(process);
        String pubKey = getKeyFromOutput(output);
        return pubKey;
    }
    private static String getPublicKeyFromPrivateKey(String privateKeyPath, String certificateKeyType) throws InvalidModelException {
        String command;
        if (certificateKeyType.equals(EC_CERTIFICATE_TYPE)) {
            command = String.format(GET_PUB_FROM_PRIV, "ec", privateKeyPath);
        } else {
            command = String.format(GET_PUB_FROM_PRIV, "rsa", privateKeyPath);
        }
        Process process = getProcess(command);
        try {
            if(!process.waitFor(1, TimeUnit.SECONDS)) {
                //timeout - kill the process.
                process.destroy(); // consider using destroyForcibly instead
                throw new InvalidModelException("Could not read public key from private key.\n" +
                                                "Make sure the key is not password protected.");
            }
        } catch (java.lang.InterruptedException e) {
            throw new InvalidModelException("Could not read public key from private key.\n" +
                                            "Make sure the key is not password protected.");
        }
        String output = getCommandOutput(process);
        if (process.exitValue() != 0) {
            throw new InvalidModelException("Invalid private key");
        }
        String pubKey = getKeyFromOutput(output);
        return pubKey;
    }
    private static String getCommandOutput(Process process) {
        String result = "";
        BufferedReader stdInput = new BufferedReader(
                new InputStreamReader(process.getInputStream())
        );
        String s;
        try {
            while ((s = stdInput.readLine()) != null) {
                result += (s + "\n");
            }
        } catch (java.io.IOException e) {
            return null;
        }
        return  result;
    }
}
