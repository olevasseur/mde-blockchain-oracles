package oracleplugin;

public class Event extends OracleInteraction implements VerifiableElement {
    SmartContract source;
    Bridge destination;

    @Override
    public Object getDestination() {
        return destination;
    }

    @Override
    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, Bridge.class);
        this.destination = (Bridge)destination;
    }

    @Override
    public Object getSource() {
        return source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, SmartContract.class);
        this.source = (SmartContract)source;
    }

    @Override
    public void checkForError() {

    }
}
