package oracleplugin;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import static oracleplugin.HelperFunctions.readAllBytesJava7;

public class CallTransaction extends OracleInteraction implements VerifiableElement {
    SmartContract destination;
    Bridge source;
    String smartContractFunctionName;

    @Override
    public Object getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, SmartContract.class);
        this.destination = (SmartContract)destination;
    }

    @Override
    public Object getSource() {
        return this.source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, Bridge.class);
        this.source = (Bridge)source;
    }

    @Override
    public void checkForError() throws InvalidModelException {
        if (this.smartContractFunctionName == null || this.smartContractFunctionName.equals("")) {
            throw new InvalidModelException("SendTransaction should have a smartContractFunctionName attribute");
        }
        String abiPath = this.destination.getAbiPath();
        String abiFileContent = readAllBytesJava7(abiPath);
        String newContent = "{\"abi\" = " + abiFileContent + "}";
        JsonObject element = new JsonParser().parse(newContent).getAsJsonObject();
        JsonArray array = element.get("abi").getAsJsonArray();
        String functionName = this.smartContractFunctionName;
        for (JsonElement el: array) {
            JsonObject jsonObject = el.getAsJsonObject();
            if (jsonObject.get("type").getAsString().equals("function")) {
                String jsonName = jsonObject.get("name").getAsString();
                if (jsonName.equals(functionName)) {
                    return;
                }
            }
        }
        throw new InvalidModelException("The attribute smartContractFunctionName is not the name of a valid smart contract method");
    }
}
