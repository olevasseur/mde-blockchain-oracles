package oracleplugin;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import static oracleplugin.HelperFunctions.readAllBytesJava7;

public class EventSubscription extends OracleInteraction implements VerifiableElement {
    private String eventName;
    private EventNotification associatedEventNotification;
    private SmartContract smartContract;
    private Bridge source;
    SendTransaction associatedTransaction = null;

    public SendTransaction getAssociatedTransaction() {
        return associatedTransaction;
    }

    public void setAssociatedTransaction(SendTransaction associatedTransaction) {
        this.associatedTransaction = associatedTransaction;
    }

    public SmartContract getDestination() {
        return smartContract;
    }

    public void setDestination(Object smartContract) throws InvalidModelException {
        this.checkDestinationType(smartContract, SmartContract.class);
        this.smartContract = (SmartContract) smartContract;
    }

    @Override
    public Object getSource() {
        return this.source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, Bridge.class);
        this.source = (Bridge)source;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public EventNotification getAssociatedEventNotification() {
        return associatedEventNotification;
    }

    public void setAssociatedEventNotification(EventNotification associatedEventNotification) {
        this.associatedEventNotification = associatedEventNotification;
    }

    public EventSubscription() {

    }
    public EventSubscription(String eventName, EventNotification associatedEventNotification) {
        this.eventName = eventName;
        this.associatedEventNotification = associatedEventNotification;
    }

    @Override
    public void checkForError() throws InvalidModelException {
        String abiPath = this.getDestination().getAbiPath();
        String abiFileContent = readAllBytesJava7(abiPath);
        String newContent = "{\"abi\" = " + abiFileContent + "}";
        JsonObject element = new JsonParser().parse(newContent).getAsJsonObject();
        JsonArray array = element.get("abi").getAsJsonArray();
        String eventName = this.getEventName();
        JsonObject jsonEvent = null;
        for (JsonElement el: array) {
            JsonObject jsonObject = el.getAsJsonObject();
            if (jsonObject.get("type").getAsString().equals("event")) {
                String jsonName = jsonObject.get("name").getAsString();
                if (jsonName.equals(eventName)) {
                    jsonEvent = jsonObject;
                    break;
                }
            }
        }
        if (jsonEvent == null) {
            throw new InvalidModelException("Smart contract event name " + this.getEventName() + " is invalid");
        }
    }
}
