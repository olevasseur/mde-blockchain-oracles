package oracleplugin;

public class TransactionReply extends OracleInteraction implements VerifiableElement {
    private Bridge destination;
    private SmartContract source;

    @Override
    public Object getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, Bridge.class);
        this.destination = (Bridge)destination;
    }

    @Override
    public Object getSource() {
        return this.source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, SmartContract.class);
        this.source = (SmartContract)source;
    }

    @Override
    public void checkForError() {

    }
}
