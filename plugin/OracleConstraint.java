package oracleplugin;

public class OracleConstraint {
    private String variableName;
    private String operator;
    private Object operand;
    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Object getOperand() {
        return operand;
    }

    public void setOperand(String operand) {
        this.operand = operand;
    }
    public OracleConstraint(String constraintBody) throws InvalidModelException {
        try {
            String [] constraintElements = constraintBody.split(" ");
            this.variableName = constraintElements[0];
            this.operator = constraintElements[1];
            boolean validOperator = this.operator.equals("==")
                                    || this.operator.equals(">=")
                                    || this.operator.equals(">")
                                    || this.operator.equals("<=")
                                    || this.operator.equals("<");
            if (! validOperator) {
                throw new InvalidModelException("Invalid trigger condition for off-chain event subscription.");
            }
            try {
                this.operand = Integer.valueOf(constraintElements[2]);
            } catch (Exception e) {
                this.operand = constraintElements[2];
            }
        } catch (Exception e) {
            throw new InvalidModelException("Invalid trigger condition for off-chain event subscription.");
        }
    }
}
