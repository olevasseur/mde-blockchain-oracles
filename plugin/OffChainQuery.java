package oracleplugin;

public class OffChainQuery extends OracleInteraction implements VerifiableElement {
    private String queryPath;
    private OffChainApplication destination;
    private Bridge source;

    public String getQueryPath() {
        return queryPath;
    }

    @Override
    public Object getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, OffChainApplication.class);
        this.destination = (OffChainApplication)destination;
    }

    @Override
    public Object getSource() {
        return this.source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, Bridge.class);
        this.source = (Bridge)source;
    }

    @Override
    public void checkForError() throws InvalidModelException {
        if (this.queryPath == null || this.queryPath.equals("")) {
            throw new InvalidModelException("OffChainQuery query path must be defined");
        }
    }
}
