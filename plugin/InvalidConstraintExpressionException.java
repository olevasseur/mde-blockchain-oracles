package oracleplugin.exceptions;

public class InvalidConstraintExpressionException extends Throwable {
    String message = "Expression must have a variable name, and operator and a value. \n" +
            "For example, \"myVariable > 10\" is a valid expression.\n";

    public String toString() {
        return this.message;
    }
}
