package oracleplugin;

public abstract class OracleInteraction {
    public abstract Object getDestination();
    public abstract void setDestination(Object destination) throws InvalidModelException;
    public abstract Object getSource();
    public abstract void setSource(Object source) throws InvalidModelException;
    public void checkSourceType(Object source, Class sourceConcreteClass) throws InvalidModelException {
        if (!source.getClass().equals(sourceConcreteClass)) {
            throw new InvalidModelException(
                    String.format("The source of %1$s should be of type %2$s",
                            this.getClass().getSimpleName(),
                            sourceConcreteClass.getSimpleName()
                    )
            );
        }
    }
    public void checkDestinationType(Object destination, Class destinationConcreteClass) throws InvalidModelException {
        if (!destination.getClass().equals(destinationConcreteClass)) {
            throw new InvalidModelException(
                String.format("The destination of %1$s should be of type %2$s",
                        this.getClass().getSimpleName(),
                        destinationConcreteClass.getSimpleName()
                )
            );
        }
    }
}
