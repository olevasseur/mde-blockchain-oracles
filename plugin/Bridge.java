package oracleplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bridge implements Comparable, VerifiableElement  {
    private static String[] validOracleTypes = {
            "pull-based inbound", "pull-based outbound", "push-based outbound", "push-based inbound"};
    private String caCertificate;
    private String serverCertificate;
    private String serverKey;
    private String generatedCodeDirectory;
    private String oracleType;
    private int port;
    private String nodeUrl;
    private List<EventSubscription> eventSubscriptionList = new ArrayList<>();
    //this map should only be used by the source code generator to generate instructions to load contracts' abis
    private Map<String, SmartContract> knownSmartContractMap = new HashMap<>();
    private List<OffChainEventSubscription> offChainEventSubscriptionList = new ArrayList<>();
    private List<SendTransaction> pullBasedInboundTransactions = new ArrayList<>();

    public void addPullBasedInboundTransaction(SendTransaction transaction) {
        this.pullBasedInboundTransactions.add(transaction);
    }

    public List<SendTransaction> getPullBasedInboundTransactions() {
        return this.pullBasedInboundTransactions;
    }

    public String getNodeUrl() {
        return this.nodeUrl;
    }

    public int getPort() {
        return port;
    }

    public String getGeneratedCodeDirectory() {
        return generatedCodeDirectory;
    }
    public List<OffChainEventSubscription> getOffChainEventSubscriptionList() {
        return offChainEventSubscriptionList;
    }

    public String getOracleType() {
        return oracleType;
    }

    public void checkForNonExistentPaths() throws InvalidModelException {
        //elements which use paths: SendTransaction, SmartContract, Bridge
        if (HelperFunctions.fileExists(this.caCertificate) != null) {
            throw new InvalidModelException("File does not exist: " + this.caCertificate);
        }
        if (HelperFunctions.fileExists(this.serverCertificate) != null) {
            throw new InvalidModelException("File does not exist: " + this.serverCertificate);
        }
        if (HelperFunctions.fileExists(this.serverKey) != null) {
            throw new InvalidModelException("File does not exist: " + this.serverKey);
        }
        this.caCertificate = this.caCertificate.replaceAll("\\\\", "\\\\\\\\");
        this.serverCertificate = this.serverCertificate.replaceAll("\\\\", "\\\\\\\\");
        this.serverKey = this.serverKey.replaceAll("\\\\", "\\\\\\\\");
    }

    private void checkOracleType() throws InvalidModelException {
        if (this.oracleType == null) {
            throw new InvalidModelException("Error: Bridge oracle type must be defined");
        }
        for (String oracleType: validOracleTypes) {
            if (this.oracleType.equals(oracleType)) {
                return;
            }
        }
        throw new InvalidModelException(String.format("Error: Bridge oracle type \"%1$s\" is not valid", this.oracleType));
    }

    public void checkChainIds() throws InvalidModelException {
        // check that all smart contracts have the same chain id
        Integer chainId = null;
        for (String key: this.knownSmartContractMap.keySet()) {
            SmartContract current = knownSmartContractMap.get(key);
            if (chainId == null) {
                chainId = current.getChainId();
            }
            if (chainId - current.getChainId() != 0) {
                throw new InvalidModelException("All smart contracts should have the same chainId");
            }
        }
    }

    @Override
    public void checkForError() throws InvalidModelException {
        checkOracleType();
        this.checkForNonExistentPaths();
        // Smart contracts are verified first, because the verification of off-chain event subscription
        // assumes that the abi files exist, which is an attribute of the smart contract
        for (String scId: this.getKnownSmartContractMap().keySet()) {
            SmartContract sc = this.getKnownSmartContractMap().get(scId);
            sc.checkForError();
        }
        for (OffChainEventSubscription oces: this.getOffChainEventSubscriptionList()) {
            oces.checkForError();
        }
        for (EventSubscription es: this.getEventSubscriptionList()) {
            es.checkForError();
        }
        ShellCommand.checkCertificateAndAssociatedKey(this.serverCertificate, this.serverKey);
    }

    public void addOffChainEventSubscription(OffChainEventSubscription oes) {
        this.offChainEventSubscriptionList.add(oes);
    }

    public String getCaCertificate() {
        return caCertificate;
    }

    public void setCaCertificate(String caCertificate) {
        this.caCertificate = caCertificate;
    }

    public String getServerCertificate() {
        return serverCertificate;
    }

    public void setServerCertificate(String serverCertificate) {
        this.serverCertificate = serverCertificate;
    }

    public String getServerKey() {
        return serverKey;
    }

    public void setServerKey(String serverKey) {
        this.serverKey = serverKey;
    }


    @Override
    public int compareTo(Object o) {
        Bridge other = (Bridge)o;
        boolean sameCaCertificate = this.caCertificate.compareTo(other.getCaCertificate()) == 0;
        boolean sameServerKey = this.serverKey.compareTo(other.getServerKey()) == 0;
        boolean sameServerCertificate = this.serverCertificate.compareTo(other.getServerCertificate()) == 0;
        boolean sameGeneratedCodePath = this.generatedCodeDirectory.compareTo(other.generatedCodeDirectory) == 0;
        boolean samePort = this.port == other.getPort();
        boolean sameNodeUrl = this.nodeUrl.equals(other.getNodeUrl());
        boolean equal =
                sameCaCertificate &&
                sameServerKey &&
                sameServerCertificate &&
                sameGeneratedCodePath &&
                samePort &&
                sameNodeUrl;
        return equal ? 0 : -1;
    }

    public Map<String, SmartContract> getKnownSmartContractMap() {
        return knownSmartContractMap;
    }

    public void addKnownSmartContract(SmartContract sc) {
        this.knownSmartContractMap.put(sc.getUniqueIdentifier(), sc);
    }

    public List<EventSubscription> getEventSubscriptionList() {
        return eventSubscriptionList;
    }

    public Bridge() {

    }

    public Bridge(String caCertificate, String serverCertificate, String serverKey) {
        this.caCertificate = caCertificate;
        this.serverCertificate = serverCertificate;
        this.serverKey = serverKey;
    }
    public void addEventSubscription(EventSubscription eventSubscription) {
        this.eventSubscriptionList.add(eventSubscription);
    }

    /**
     * Merges other bridge into instance
     * @param other
     * @throws InvalidModelException
     */
    public void mergeBridge(Bridge other) throws InvalidModelException {
        if (this.compareTo(other) != 0) {
            throw new InvalidModelException("The bridges of all diagrams must have the same attributes.");
        }
        for (EventSubscription e: other.getEventSubscriptionList()) {
            this.addEventSubscription(e);
        }
        //since it is a map, adding the same smart contract twice will not cause any problem
        //because
        // this map should only be used for loading the abis when generating the source code
        for (String scKey: other.getKnownSmartContractMap().keySet()) {
            Map<String, SmartContract> smartContractsOne = this.getKnownSmartContractMap();
            if (!smartContractsOne.containsKey(scKey)) {
                smartContractsOne.put(scKey, other.getKnownSmartContractMap().get(scKey));
            }
        }
        this.offChainEventSubscriptionList.addAll(other.getOffChainEventSubscriptionList());
        this.checkChainIds();
    }

}
