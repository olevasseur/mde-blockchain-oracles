package oracleplugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class HelperFunctions {
    public static String readAllBytesJava7(String filePath)
    {
        String content = "";

        try
        {
            content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return content;
    }

    public static String fileExists(String path) {
        File file = new File(path);
        if (!file.exists() || file.isDirectory()) return path;
        return null;
    }

    public static String readFileFromPackage(String path) {
        InputStream inputStream = HelperFunctions.class.getResourceAsStream(path);
        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";
        return result;
    }
}
