package oracleplugin.exceptions;

public class InvalidElementException extends Throwable {
    public InvalidElementException(String errorMessage) {
        super(errorMessage);
    }
}
