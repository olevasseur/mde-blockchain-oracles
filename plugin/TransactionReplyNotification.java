package oracleplugin;

public class TransactionReplyNotification extends OracleInteraction implements VerifiableElement {
    private Bridge source;
    private OffChainApplication destination;
    @Override
    public Object getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, OffChainApplication.class);
        this.destination = (OffChainApplication) destination;
    }

    @Override
    public Object getSource() {
        return this.source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, Bridge.class);
        this.source = (Bridge)source;
    }

    @Override
    public void checkForError() {
    }
}
