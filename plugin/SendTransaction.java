package oracleplugin;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static oracleplugin.HelperFunctions.readAllBytesJava7;

public class SendTransaction extends OracleInteraction implements VerifiableElement {
    private String privateKey;
    private String smartContractFunctionName;
    private Integer gasPrice;
    private Integer gasLimit;
    private Integer numberOfConfirmations = 12;
    private String confirmationPath;
    private SmartContract destination;
    private Double value;
    private List<TransactionArgument> argumentList = new ArrayList<>();
    private Bridge source;
    public List<TransactionArgument> getArgumentList() {
        return argumentList;
    }

    public void addArgument(TransactionArgument a) {
        this.argumentList.add(a);
    }

    @Override
    public SmartContract getDestination() {
        return destination;
    }

    public void setDestination(Object destination) throws InvalidModelException {
        this.checkDestinationType(destination, SmartContract.class);
        this.destination = (SmartContract)destination;
    }

    @Override
    public Object getSource() {
        return this.source;
    }

    @Override
    public void setSource(Object source) throws InvalidModelException {
        this.checkSourceType(source, Bridge.class);
        this.source = (Bridge)source;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getSmartContractFunctionName() {
        return smartContractFunctionName;
    }

    public void setSmartContractFunctionName(String smartContractFunctionName) {
        this.smartContractFunctionName = smartContractFunctionName;
    }

    public Integer getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(int gasPrice) {
        this.gasPrice = gasPrice;
    }

    public Integer getGasLimit() {
        return gasLimit;
    }

    public void setGasLimit(int gasLimit) {
        this.gasLimit = gasLimit;
    }

    public Integer getNumberOfConfirmations() {
        return numberOfConfirmations;
    }

    public void setNumberOfConfirmations(int numberOfConfirmations) {
        this.numberOfConfirmations = numberOfConfirmations;
    }

    public String getConfirmationPath() {
        return confirmationPath;
    }

    public void setConfirmationPath(String confirmationPath) {
        this.confirmationPath = confirmationPath;
    }
    public Double getValue() {
        return value;
    }

    public SendTransaction() {

    }
    public SendTransaction(String privateKey, String smartContractFunctionName) {
       this.privateKey = privateKey;
       this.smartContractFunctionName = smartContractFunctionName;
    }

    private void checkForNonExistentPaths() throws InvalidModelException {
        if (HelperFunctions.fileExists(this.privateKey) != null) {
            throw new InvalidModelException(this.privateKey + " is not a valid private key path");
        }
    }

    /**
     * This function does not check that all the smart contract parameters are present, because it is
     * possible that this information is calculated by the off-chain application and cannot be known
     * in advance
      * @throws InvalidModelException
     */
    public void checkForError() throws InvalidModelException {
        if (this.privateKey == null || this.privateKey.equals("")) {
            throw new InvalidModelException("SendTransaction should have a privateKey attribute");
        }
        if (this.smartContractFunctionName == null || this.smartContractFunctionName.equals("")) {
            if (this.value == null || this.value == 0) {
                throw new InvalidModelException("SendTransaction should have a smartContractFunctionName attribute");
            }
        } else {
            // get smart contract function associated with transaction
            JsonObject jsonFunction = getSmartContractFunctionAsJsonObject();
            if (jsonFunction == null) {
                throw new InvalidModelException("Smart contract function name " + this.smartContractFunctionName + " is invalid");
            }
            // get associated smart contract function parameters
            JsonArray inputs = jsonFunction.get("inputs").getAsJsonArray();
            Map<String, JsonObject> jsonArguments = new HashMap<>();
            for (JsonElement arg: inputs) {
                JsonObject argObj = arg.getAsJsonObject();
                jsonArguments.put(argObj.get("name").getAsString(), argObj);
            }
            // check that all parameters of transaction exist in ABI
            Map<String, TransactionArgument> taMap = new HashMap<>();
            for (TransactionArgument ta: this.getArgumentList()) {
                taMap.put(ta.getSCTransactionArgumentName(), ta);
                if (!jsonArguments.containsKey(ta.getSCTransactionArgumentName())) {
                    throw new InvalidModelException("Transaction parameter " + ta.getSCTransactionArgumentName() + " is invalid.");
                }
            }
        }
        if (this.confirmationPath != null) {
            try {
                String [] splitOnPath = this.confirmationPath.split("/");
                String [] splitOnPort = splitOnPath[0].split(":");
                String path = "/" + splitOnPath[1]; // only used to trigger exception if doesn't exist
                String hostname = splitOnPort[0]; // only used to trigger exception if doesn't exist
                String port = splitOnPort[1]; // only used to trigger exception if doesn't exist
            } catch (Exception e) {
                throw new InvalidModelException(
                        "SendTransaction confirmationPath attribute should have the following format : " +
                        "hostname:port/path"
                );
            }
        }
        this.checkForNonExistentPaths();
        if (!this.privateKey.contains("\\\\")) {
            this.privateKey = this.privateKey.replaceAll("\\\\", "\\\\\\\\");
        }
    }
    public List<String> getOrderedParameterNames() {
        List<String> result = new ArrayList<>();
        JsonObject jsonFunction = getSmartContractFunctionAsJsonObject();
        JsonArray inputs = jsonFunction.get("inputs").getAsJsonArray();
        for (JsonElement arg: inputs) {
            JsonObject argObj = arg.getAsJsonObject();
            result.add(argObj.get("name").getAsString());
        }
        return result;
    }
    private JsonObject getSmartContractFunctionAsJsonObject() {
        String abiPath = this.destination.getAbiPath();
        String abiFileContent = readAllBytesJava7(abiPath);
        String newContent = "{\"abi\" = " + abiFileContent + "}";
        JsonObject element = new JsonParser().parse(newContent).getAsJsonObject();
        JsonArray array = element.get("abi").getAsJsonArray();
        String functionName = this.getSmartContractFunctionName();
        JsonObject jsonFunction = null;
        for (JsonElement el: array) {
            JsonObject jsonObject = el.getAsJsonObject();

            if (jsonObject.get("type").getAsString().equals("function")) {
                String jsonName = jsonObject.get("name").getAsString();
                if (jsonName.equals(functionName)) {
                    jsonFunction = jsonObject;
                    break;
                }
            }
        }
        return jsonFunction;
    }

}
