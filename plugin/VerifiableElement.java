package oracleplugin;

public interface VerifiableElement {
    public void checkForError() throws InvalidModelException;
}
