package oracleplugin.exceptions;

public class ModelVerificationException extends Throwable {
    public String toString() {
        return this.getMessage();
    }
    public ModelVerificationException(String message) {
        super(message);
    }
}
