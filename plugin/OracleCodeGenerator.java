package oracleplugin;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static oracleplugin.HelperFunctions.readAllBytesJava7;
import static oracleplugin.HelperFunctions.readFileFromPackage;

public class OracleCodeGenerator {

    private static String FILE_TEMPLATE = "";
    private static String EVENT_TEMPLATE = "";
    private static String OCES_TEMPLATE = "";

    private static final String FILE_TEMPLATE_PATH = "templates/oracle_template.txt";
    private static final String EVENT_TEMPLATE_PATH = "templates/smart_contract_event_template.txt";
    private static final String OCES_FUNCTION_TEMPLATE_PATH = "templates/off_chain_subscription_template.txt";
    // oces is short for Off-chain event subscription
    private static final String OCES_TRANSACTION_CONFIRMATION = "templates/oces_transaction_confirmation.txt";
    private static final String EVENT_ASSOCIATED_TRANSACTION_TEMPLATE_PATH = "templates/event_associated_transaction_template.txt";

    private static final String JSON_CONTRACTS_DICT_TEMPLATE = "jsonContracts['%s'] = JSON.parse(fs.readFileSync('%s'));\n\n";
    private static final String WEB3_CONTRACTS_DICT_TEMPLATE = "web3Contracts['%1$s'] = new web3.eth.Contract(jsonContracts['%1$s'], '%1$s');\n" +
                                                               "web3Contracts['%1$s'].events.%2$s%3$s\n\n";
    //dictionary that contains functions that represent the off-chain event subscription
    private static final String OCES_FUNCTIONS_DICT_TEMPLATE = "offChainEventSubscriptions['%1$s']=";
    private static final String OCES_FUNCTION_CALL_TEMPLATE = "offChainEventSubscriptions['%1$s']([offChainEventSubscriptions['%1$s'], %2$s]);";
    //    public static final String TRANSACTION_ATTRIBUTE_DICT_TEMPLATE = "transactionAttributes['%1$s'] = {};\n";
    private static final String TRANSACTION_ATTRIBUTE_TEMPLATE = "%3$sattributes['%1$s'] = %2$s;\n";
    //    private static final String TRANSACTION_ARGUMENT_DICT_TEMPLATE = "transactionAttributes['%1$s']['arguments'] = {}\n";
    private static final String TRANSACTION_ARGUMENT_TEMPLATE = "transactionAttributes['%1$s']['arguments']['%2$s'] = %3$s;\n";


    private static String getPathInPackage(String relativePath) {
        return HelperFunctions.class.getResource(relativePath).getPath();
    }
    /**
     *
     * @param bridge
     * @return String representing the code of the bridge
     * - Iterates through smart contracts known to bridge and adds contract definitions in a javascript dictionary
     * - Iterates through event subscriptions known to bridge, creates contract objects using web3.js and puts them in a dictionary
     * -
     */
    public static String generateOracleCode(Bridge bridge) {
        try {
            FILE_TEMPLATE = readFileFromPackage(FILE_TEMPLATE_PATH);
            EVENT_TEMPLATE = readFileFromPackage(EVENT_TEMPLATE_PATH);
            OCES_TEMPLATE = readFileFromPackage(OCES_FUNCTION_TEMPLATE_PATH);
            String result = String.format(FILE_TEMPLATE,
                    bridge.getServerKey(), // 1
                    bridge.getServerCertificate(), // 2
                    bridge.getCaCertificate(), // 3
                    bridge.getPort(), // 4
                    bridge.getNodeUrl() // 5
            );
            Map<String, SmartContract> smartContractMap = bridge.getKnownSmartContractMap();
            result += "// Add contract definitions (ABI) to `jsonContracts` dictionary\n";
            for (String key: smartContractMap.keySet()) {
                SmartContract sc = smartContractMap.get(key);
                result += String.format(JSON_CONTRACTS_DICT_TEMPLATE, sc.getContractAddress(), sc.getAbiPath());
            }
//            result += "// read transaction attributes into dictionary\n";
//            result += getTransactionAttributes(pullBasedInboundTransactions);
//            result += getTransactionArguments(pullBasedInboundTransactions);
            result += "// Add web3 contract instances to `web3Contracts` dictionary\n";
            for (EventSubscription es: bridge.getEventSubscriptionList()) {
                SmartContract sc = es.getDestination();
                EventNotification eventNotification = es.getAssociatedEventNotification();
                OffChainApplication receiver =  (OffChainApplication) eventNotification.getDestination();
                SendTransaction associatedTransaction = es.getAssociatedTransaction();
                String eventAssociatedTransaction;
                if (associatedTransaction == null) {
                    eventAssociatedTransaction = "";
                } else {
                    final String EVENT_ASSOCIATIED_TRANSACTION_TEMPLATE =
                            readFileFromPackage(EVENT_ASSOCIATED_TRANSACTION_TEMPLATE_PATH);
                    String transactionAttributes = getTransactionAttributes(associatedTransaction, "\t\t\t\t");
                    String transactionParameters = getTransactionParameters(associatedTransaction, "\t\t\t\t");
                    eventAssociatedTransaction = String.format(EVENT_ASSOCIATIED_TRANSACTION_TEMPLATE, transactionAttributes, transactionParameters);
                }
                String event = String.format(
                        EVENT_TEMPLATE,
                        receiver.getHost(),
                        eventNotification.getNotificationPath(),
                        receiver.getPort(),
                        eventAssociatedTransaction);
                String eventCreation = String.format(
                        WEB3_CONTRACTS_DICT_TEMPLATE,
                        sc.getContractAddress(),
                        es.getEventName(),
                        event);
                result += eventCreation;
            }

            for (OffChainEventSubscription oces: bridge.getOffChainEventSubscriptionList()) {
                String ocesString = String.format(OCES_FUNCTIONS_DICT_TEMPLATE, oces.getId());
                OffChainApplication dataSource = oces.getDataSource();
                OffChainQuery offChainQuery = oces.getOffChainQuery();
                SendTransaction transaction = oces.getAssociatedTransaction();
                List<SendTransaction> transactionList = new ArrayList<>();
                transactionList.add(transaction);
//                result += getTransactionAttributes(transactionList);
//                result += getTransactionArguments(transactionList);
                String parametersString= getTransactionParameters(transaction, "\t\t\t\t"); // generates instructions to add parameters values to array

                SmartContract sc = transaction.getDestination();
                OracleConstraint constraint = oces.getConstraints().get(0);
                Object operand = constraint.getOperand();
                String operandString = operand instanceof String? "'" + operand.toString() + "'": operand.toString();
                String transactionConfirmationCode = "";
                if (transaction.getConfirmationPath() != null) {
                    transactionConfirmationCode = HelperFunctions
                            .readFileFromPackage(OCES_TRANSACTION_CONFIRMATION);
                }
                String transactionAttributes = getTransactionAttributes(transaction, "\t\t\t\t");
                ocesString += String.format(OCES_TEMPLATE,
                        dataSource.getHost(), // 1
                        dataSource.getPort(), // 2
                        offChainQuery.getQueryPath(), // 3
                        constraint.getVariableName(), // 4
                        constraint.getOperator(), // 5
                        operandString, // 6
                        parametersString, // 7
                        transactionConfirmationCode, // 8
                        transactionAttributes // 9
                );
                int intervalMilliseconds = oces.getQueryInterval() * 1000;
                ocesString += String.format(OCES_FUNCTION_CALL_TEMPLATE, oces.getId(), intervalMilliseconds);
                result += ocesString;
            }

            return result;
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    //write transaction attributes from model to a dictionary so they can be accessible
    //from the oracle
    private static String getTransactionAttributes(SendTransaction transaction, String indent) {
        String result = "let attributes = {};\n";

        //mandatory fields
        String privateKeyPath = transaction.getPrivateKey();
        String privateKeyAttributeValue = String.format("fs.readFileSync('%1$s').toString()", privateKeyPath);
        result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                "privateKey", privateKeyAttributeValue, indent);
        //optional fields
        if (transaction.getGasLimit() != null) {
            result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                    "gas", transaction.getGasLimit(), indent);
        }
        if (transaction.getGasPrice() != null) {
            result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                    "gasPrice", transaction.getGasPrice(), indent);
        }
        if (transaction.getConfirmationPath() != null) {
            result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                    "confirmationPath", "'" + transaction.getConfirmationPath() + "'", indent);
        }
        if (transaction.getConfirmationPath() != null) {
            result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                    "numberOfConfirmations", transaction.getNumberOfConfirmations(), indent);
        }
        if (transaction.getValue() != null && transaction.getValue() > 0) {
            result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                    "value", transaction.getValue(), indent);
        }
        if (transaction.getSmartContractFunctionName() != null) {
            result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                    "functionName",
                    "'" + transaction.getSmartContractFunctionName() + "'", indent);
        }
        result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE,
                "contractAddress",
                "'" + transaction.getDestination().getContractAddress() + "'", indent);
        result += String.format(TRANSACTION_ATTRIBUTE_TEMPLATE, "chainId", transaction.getDestination().getChainId(), indent);

        return result;
    }
    private static String getTransactionParameters(SendTransaction transaction, String indentation) {
        String parametersString = "let parameters = {};\n";
        for (TransactionArgument ta: transaction.getArgumentList()){
            if (ta.isQueryResponseArgument()) {
                parametersString += String.format("%3$slet %1$s = jsonObj.%1$s; \n", ta.getValue().toString(), indentation);
                parametersString += String.format("%3$sparameters.%1$s(%2$s); \n",
                        ta.getSCTransactionArgumentName(),
                        ta.getValue().toString(),
                        indentation);

            } else {
                parametersString += String.format("%3$sparameters.%1$s=%2$s; \n", ta.getSCTransactionArgumentName(), ta.getValueString(), indentation);
            }
        }
        return parametersString;
    }
    //    private static String getTransactionArguments(List<SendTransaction> transactionList) {
//        String result= "";
//
//        for (SendTransaction transaction: transactionList) {
//            String key = transaction.getDestination().getContractAddress() + transaction.getSmartContractFunctionName();
//            result += String.format(TRANSACTION_ARGUMENT_DICT_TEMPLATE, key);
//            for (TransactionArgument ta: transaction.getArgumentList()) {
//                Object value = ta.getValue();
//                if (!isInt(value)) {
//                    value = "'" + value.toString() + "'";
//                }
//                result+= String.format(TRANSACTION_ARGUMENT_TEMPLATE,
//                        key, ta.getSCTransactionArgumentName(), value);
//            }
//        }
//        return result;
//    }
    private static boolean isInt(Object obj) {
        try {
            Integer test = Integer.valueOf(obj.toString());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}