package oracleplugin;

public class TransactionArgument {
    private String SCTransactionArgumentName;
    private boolean isQueryResponseArgument = false;
    private Object value;

    public String getSCTransactionArgumentName() {
        return SCTransactionArgumentName;
    }

    public void setSCTransactionArgumentName(String SCTransactionArgumentName) {
        this.SCTransactionArgumentName = SCTransactionArgumentName;
    }

    public boolean isQueryResponseArgument() {
        return isQueryResponseArgument;
    }

    public void setQueryResponseArgument(boolean queryResponseArgument) {
        isQueryResponseArgument = queryResponseArgument;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
    public String getValueString() {
        try {
            Integer.valueOf(this.value.toString()); // if it is an integer, do not add quotes
            return this.value.toString();
        } catch (Exception e) {
            return String.format("'%1$s'", this.value.toString());
        }

    }
}
