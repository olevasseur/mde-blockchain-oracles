package oracleplugin;

public class SmartContract implements VerifiableElement {
    private String abiPath;
    private int chainId;
    private String contractAddress;

    public String getAbiPath() {
        return abiPath;
    }

    public void setAbiPath(String abiPath) {
        this.abiPath = abiPath;
    }

    public int getChainId() {
        return chainId;
    }

    public void setChainId(int chainId) {
        this.chainId = chainId;
    }

    public String getContractAddress() {
        return contractAddress;
    }

    public void setContractAddress(String contractAddress) {
        this.contractAddress = contractAddress;
    }
    public String getUniqueIdentifier() {
        return this.getContractAddress() + this.getChainId();
    }

    private void checkForNonExistentPaths() throws InvalidModelException {
        if (HelperFunctions.fileExists(this.abiPath) != null) {
            throw new InvalidModelException(this.abiPath + " is not a valid abi file path");
        }
        this.abiPath = this.abiPath.replace("\\", "\\\\");
    }

    @Override
    public void checkForError() throws InvalidModelException {
        this.checkForNonExistentPaths();
    }
}
