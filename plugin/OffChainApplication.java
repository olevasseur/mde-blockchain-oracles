package oracleplugin;

public class OffChainApplication implements VerifiableElement {
    private String host;
    private Integer port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public OffChainApplication() {

    }

    @Override
    public void checkForError() throws InvalidModelException {
        if (this.port == null) {
            throw new InvalidModelException("Error: OffChainApplication port must be defined");
        }
        if (this.host == null) {
            throw new InvalidModelException("Error: OffChainApplication host must be defined");
        }
    }
}
