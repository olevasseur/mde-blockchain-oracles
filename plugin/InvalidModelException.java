package oracleplugin;

public class InvalidModelException extends Throwable {
    public InvalidModelException(String errorMessage) {
        super(errorMessage);
    }
}
