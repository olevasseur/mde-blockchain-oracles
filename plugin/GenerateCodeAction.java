package oracleplugin;


import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.actions.MDAction;
import com.nomagic.magicdraw.ui.dialogs.MDDialogParentProvider;
import com.nomagic.ui.ScalableImageIcon;

import javax.annotation.CheckForNull;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.*;

import java.io.FileWriter;
import java.lang.Class;
import java.util.*;

import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Package;
import com.nomagic.uml2.ext.magicdraw.interactions.mdbasicinteractions.*;
import com.nomagic.uml2.ext.magicdraw.interactions.mdfragments.CombinedFragment;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;

import java.io.IOException;  // Import the IOException class to handle errors
import java.lang.reflect.*;

class GenerateCodeAction extends MDAction
{
    public static final String OPAQUE_EXPRESSION_CLASS_NAME = "OpaqueExpressionImpl";
    public static final String COLLABORATION_CLASS_NAME = "CollaborationImpl";
    public static final String INTERACTION_CLASS_NAME = "InteractionImpl";
    public static final String SMART_CONTRACT_STEREOTYPE_NAME = "SmartContract";
    public static final String BRIDGE_STEREOTYPE_NAME = "Bridge";
    public static final String OFF_CHAIN_APPLICATION_STEREOTYPE_NAME = "OffChainApplication";
    public static final String EVENT_SUBSCRIPTION_STEREOTYPE_NAME = "EventSubscription";
    public static final String EVENT_STEREOTYPE_NAME = "Event";
    public static final String EVENT_NOTIFICATION_STEREOTYPE_NAME = "EventNotification";
    public static final String OFF_CHAIN_EVENT_SUBSCRIPTION_STEREOTYPE_NAME = "OffChainEventSubscription";
    public static final String OFF_CHAIN_QUERY_STEREOTYPE_NAME = "OffChainQuery";
    public static final String OFF_CHAIN_QUERY_REPLY_STEREOTYPE_NAME = "OffChainQueryReply";
    public static final String SEND_TRANSACTION_STEREOTYPE_NAME = "SendTransaction";
    public static final String CALL_TRANSACTION_STEREOTYPE_NAME = "CallTransaction";
    public static final String TRANSACTION_REPLY_STEREOTYPE_NAME = "TransactionReply";
    public static final String TRANSACTION_REQUEST_STEREOTYPE_NAME = "TransactionRequest";
    public static final String TRANSACTION_REPLY_NOTIFICATION_STEREOTYPE_NAME = "TransactionReplyNotification";
    public static final String ORACLE_PROFILE_NAME = "OracleProfile";
    public static final String LIFELINE_CLASS_NAME = "LifelineImpl";
    public static final String MESSAGE_CLASS_NAME = "MessageImpl";
    public static final String COMBINED_FRAGMENT_CLASS_NAME = "CombinedFragmentImpl";
    public static final String MESSAGE_OCCURRENCE_SPECIFICATION_IMPL = "MessageOccurrenceSpecificationImpl";
    public static final String COMBINED_FRAGMENT_IMPL = "CombinedFragmentImpl";
    public static final String PULL_BASED_INBOUND_ORACLE_NAME = "pull-based inbound";
    public static final String PUSH_BASED_INBOUND_ORACLE_NAME = "push-based inbound";
    public static final String PULL_BASED_OUTBOUND_ORACLE_NAME = "pull-based outbound";
    public static final String PUSH_BASED_OUTBOUND_ORACLE_NAME = "push-based outbound";
    public static final String MESSAGES_ERROR_TEMPLATE = "The element at position %1$s of %2$s should be %3$s.";


    GenerateCodeAction(String id, String name, @CheckForNull KeyStroke stroke, @CheckForNull String group)
    {
        super(id, name, stroke, group);
    }

    /**
     * Creates action with name "Generate Oracle source code", and with key stroke E +CTRL+SHIFT
     */
    public GenerateCodeAction()
    {
        super("Generate Oracle source code", "Generate Oracle source code", KeyStroke.getKeyStroke(KeyEvent.VK_E, NMAction.MENU_SHORTCUT_MASK + KeyEvent.SHIFT_MASK), null);
        setLargeIcon(new ScalableImageIcon(getClass(), "icons/main_toolbar_icon.gif"));
    }

    /**
     * Method is called when action should be performed. Showing simple message.
     *
     * @param e event causes action call, which is the green button in the main toolbar
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        try {
            String generationPath = generateCode(Application.getInstance().getProject().getPrimaryModel());
            JOptionPane.showMessageDialog(MDDialogParentProvider.getProvider().getDialogParent(), "Code generated at " + generationPath);
        } catch (InvalidModelException exception) {
            String exceptionMessage = exception.getMessage();
            exceptionMessage += "\nThe source code could not be generated. Please fix the diagram before trying again.";
            JOptionPane.showMessageDialog(MDDialogParentProvider.getProvider().getDialogParent(), exceptionMessage);
            return;
        }
    }

    private void createFile(String fileContent, String outputPath) {
        try {
            FileWriter myObj = new FileWriter(outputPath, false);
            myObj.write(fileContent);
            myObj.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }


    private String generateCode(Package pkg)
            throws InvalidModelException {

        List<Element> collaborations = getElementsWithClassName(pkg.getOwnedElement().iterator(), COLLABORATION_CLASS_NAME);
        List<Element> interactions = new ArrayList<Element>();
        for (Element el: collaborations) {
            List<Element> currentInteractions = getElementsWithClassName(el.getOwnedElement().iterator(), INTERACTION_CLASS_NAME);
            interactions.addAll(currentInteractions);
        }
        Bridge model = null;
        for (Element interaction: interactions) {
            try {
                Bridge current = extractSequenceDiagramElements((Interaction)interaction);
                if (model == null) {
                    model = current;
                } else {
                    model.mergeBridge(current);
                }
            } catch (InvalidModelException e) {
                String message = e.getMessage();
                String newMessage = "Diagram " + ((Interaction) interaction).getName() + ": " + message;
                throw new InvalidModelException(newMessage);
            }
        }
        String fileContent = OracleCodeGenerator.generateOracleCode(model);
        String separator = System.getProperty("file.separator");
        String generatedPath = model.getGeneratedCodeDirectory() + separator + "oracle.js";
        createFile(fileContent, generatedPath);
        return generatedPath;
    }

    /**
     * Creates all relevant Java objects from the sequence diagram of the model.
     * Deduces which kind of oracle is represented by the diagram. Based on deduction,
     * adds final associations between elements so that the bridge is ready for code generation.
     * @param sequenceDiagram the diagram of the model
     * @return the bridge ready for code generation
     * @throws InvalidModelException
     */
    private Bridge extractSequenceDiagramElements(Interaction sequenceDiagram)
            throws InvalidModelException {


        Map<String, Object> lifelineObjectIdMap = getLifelineObjects(sequenceDiagram);
        Map<String, Lifeline> lifelineElementIdMap = getLifelineElements(sequenceDiagram);
        verifyLifelines(lifelineObjectIdMap);
        Map<String, Object> messageDestinations = new HashMap<>();
        Map<String, Object> messageSources = new HashMap<>();
        List<Message> messageElements = new ArrayList<>();
        setMessagesSourcesAndDestinations(lifelineElementIdMap, lifelineObjectIdMap, messageSources, messageDestinations);
        Bridge bridgeObject = getBridgeObject(lifelineObjectIdMap);
        Lifeline bridgeElement = getBridgeElement(sequenceDiagram);
        List<OracleInteraction> orderedMessages = getOrderedMessages(bridgeElement, messageElements, messageSources, messageDestinations);
        String oracleType = bridgeObject.getOracleType();

        if (oracleType.equals(PULL_BASED_INBOUND_ORACLE_NAME)) {
            verifyPullBasedInboundOracleMessages(orderedMessages);
            EventSubscription es = (EventSubscription) orderedMessages.get(0);
            EventNotification eventNotification = (EventNotification) orderedMessages.get(2);
            es.setAssociatedEventNotification(eventNotification);
            bridgeObject.addEventSubscription(es);
            SendTransaction transaction = (SendTransaction) orderedMessages.get(4);
            Message transactionElement = messageElements.get(4);
            parseSCTransactionArguments(transactionElement, transaction);
            es.setAssociatedTransaction(transaction);
            ((VerifiableElement)transaction).checkForError(); // check transaction parameters
        } else if (oracleType.equals(PUSH_BASED_INBOUND_ORACLE_NAME)) {
            verifyPushBasedInboundOracleMessages(orderedMessages);
            OffChainQuery dataSource = (OffChainQuery) orderedMessages.get(0);
            OffChainEventSubscription ocesObject = getOffChainEventSubscription(sequenceDiagram, orderedMessages);
            ocesObject.setDataSource((OffChainApplication) dataSource.getDestination());
            SendTransaction transaction = (SendTransaction) orderedMessages.get(2);
            ocesObject.setAssociatedTransaction(transaction);
            bridgeObject.addOffChainEventSubscription(ocesObject);
            Message transactionElement = messageElements.get(2);
            parseSCTransactionArguments(transactionElement, transaction);
            ((VerifiableElement)transaction).checkForError(); // check transaction parameters
        } else if (oracleType.equals(PULL_BASED_OUTBOUND_ORACLE_NAME)) {
            verifyPullBasedOutboundOracleMessages(orderedMessages);
        } else {
            verifyPushBasedOutboundOracleMessage(orderedMessages);
            EventSubscription es = (EventSubscription) orderedMessages.get(0);
            EventNotification eventNotification = (EventNotification) orderedMessages.get(2);
            es.setAssociatedEventNotification(eventNotification);
            bridgeObject.addEventSubscription(es);
        }
        addSmartContractsReferencesToBridge(bridgeObject, lifelineObjectIdMap);
        bridgeObject.checkForError();
        return bridgeObject;
    }

    private void verifyPushBasedOutboundOracleMessage(List<OracleInteraction> orderedMessages) throws InvalidModelException {
        String [] desiredOrder = {
                EVENT_SUBSCRIPTION_STEREOTYPE_NAME,
                EVENT_STEREOTYPE_NAME,
                EVENT_NOTIFICATION_STEREOTYPE_NAME
        };
        checkMessages(desiredOrder, orderedMessages, PUSH_BASED_OUTBOUND_ORACLE_NAME);
    }

    private void verifyPullBasedOutboundOracleMessages(List<OracleInteraction> orderedMessages) throws InvalidModelException {
        String [] desiredOrder = {
                TRANSACTION_REQUEST_STEREOTYPE_NAME,
                CALL_TRANSACTION_STEREOTYPE_NAME,
                TRANSACTION_REPLY_STEREOTYPE_NAME,
                TRANSACTION_REPLY_NOTIFICATION_STEREOTYPE_NAME
        };
        checkMessages(desiredOrder, orderedMessages, PULL_BASED_OUTBOUND_ORACLE_NAME);

    }
    private void verifyPushBasedInboundOracleMessages(List<OracleInteraction> orderedMessages) throws InvalidModelException {
        String [] desiredOrder = {
                OFF_CHAIN_QUERY_STEREOTYPE_NAME,
                OFF_CHAIN_QUERY_REPLY_STEREOTYPE_NAME,
                SEND_TRANSACTION_STEREOTYPE_NAME
        };
        checkMessages(desiredOrder, orderedMessages, PUSH_BASED_INBOUND_ORACLE_NAME);

    }

    private void verifyPullBasedInboundOracleMessages(List<OracleInteraction> orderedMessages) throws InvalidModelException {
        String [] desiredOrder = {
                EVENT_SUBSCRIPTION_STEREOTYPE_NAME,
                EVENT_STEREOTYPE_NAME,
                EVENT_NOTIFICATION_STEREOTYPE_NAME,
                TRANSACTION_REQUEST_STEREOTYPE_NAME,
                SEND_TRANSACTION_STEREOTYPE_NAME
        };
        checkMessages(desiredOrder, orderedMessages, PULL_BASED_INBOUND_ORACLE_NAME);
    }

    private void checkMessages(
            String [] desiredOrder,
            List<OracleInteraction> orderedMessages,
            String oracleName) throws InvalidModelException {
        int desiredSize = desiredOrder.length;
        if (orderedMessages.size() != desiredSize) {
            throw new InvalidModelException(
                    oracleName
                    + String.format(" should contain %s messages.", desiredSize)
            );
        }

        for (int i = 0; i < desiredSize; i++) {
            VerifiableElement current = (VerifiableElement)orderedMessages.get(i);
            current.checkForError();
            String desiredInstanceClass = desiredOrder[i];
            if (!current.getClass().getSimpleName().equals(desiredInstanceClass)) {
                throw new InvalidModelException(
                        String.format(
                                MESSAGES_ERROR_TEMPLATE,
                                i+1,
                                oracleName,
                                desiredInstanceClass
                        )
                );
            }
        }
    }

    private void setMessagesSourcesAndDestinations(
            Map<String, Lifeline> lifelineElements,
            Map<String, Object> lifelineObjects,
            Map<String, Object> messageSources,
            Map<String, Object> messageDestinations) {
        for (String key: lifelineElements.keySet()) {
            Lifeline current = lifelineElements.get(key);
            Object lifelineObject = lifelineObjects.get(current.getID());
            for (InteractionFragment fragment: current.getCoveredBy()) {
                if (!fragment.getClass().getSimpleName().equals(MESSAGE_OCCURRENCE_SPECIFICATION_IMPL)) continue;
                MessageOccurrenceSpecification mos = (MessageOccurrenceSpecification) fragment;
                Message messageOfSendEvent = mos.get_messageOfSendEvent();
                Message messageOfReceiveEvent = mos.get_messageOfReceiveEvent();
                if (messageOfSendEvent != null) {
                    messageSources.put(messageOfSendEvent.getID(), lifelineObject);
                } else {
                    messageDestinations.put(messageOfReceiveEvent.getID(), lifelineObject);
                }
            }
        }
    }

    private List<OracleInteraction> getOrderedMessages(
            Lifeline bridgeElement,
            List<Message> messageElements,
            Map<String, Object> messageSources,
            Map<String, Object> messageDestinations) throws InvalidModelException {
        List<OracleInteraction> messages = new ArrayList<>();
        for (InteractionFragment fragment: bridgeElement.getCoveredBy()) {
            if (!fragment.getClass().getSimpleName().equals(MESSAGE_OCCURRENCE_SPECIFICATION_IMPL)) continue;
            MessageOccurrenceSpecification mos = (MessageOccurrenceSpecification)fragment;
            Message message = null;
            if (mos.get_messageOfSendEvent() != null) {
                message = mos.get_messageOfSendEvent();
            } else {
                message = mos.get_messageOfReceiveEvent();
            }
            messageElements.add(message);
            Stereotype stereotype = getElementStereotype(message);
            String lifelineStereotypeName = stereotype.getName();
            List<Tuple> lifelineAttributes = getTaggedValues(message, stereotype);
            OracleInteraction messageObject = (OracleInteraction)
                    createClassObjectUsingReflection(
                            lifelineStereotypeName,
                            lifelineAttributes);
            messageObject.setSource(messageSources.get(message.getID()));
            messageObject.setDestination(messageDestinations.get(message.getID()));
            messages.add(messageObject);
        }
        return messages;
    }

    private void addSmartContractsReferencesToBridge(Bridge bridge, Map<String, Object> lifelineObjectIdMap) {
        for (String id: lifelineObjectIdMap.keySet()) {
            Object lifeline = lifelineObjectIdMap.get(id);
            if (lifeline instanceof SmartContract) {
                bridge.addKnownSmartContract((SmartContract)lifeline);
            }
        }
    }

    private Lifeline getBridgeElement(Interaction sequenceDiagram) throws InvalidModelException {
        for (Element element: sequenceDiagram.getOwnedElement()) {
            if (element.getClass().getSimpleName().equals(LIFELINE_CLASS_NAME)) {
                Stereotype stereotype = getElementStereotype(element);
                String lifelineStereotypeName = stereotype.getName();
                if (lifelineStereotypeName.equals(BRIDGE_STEREOTYPE_NAME)) {
                    return (Lifeline)element;
                }
            }
        }
        return null;
    }

    private Bridge getBridgeObject(Map<String, Object> lifelineObjetIdMap) {
        Bridge bridge = null;
        for (String id: lifelineObjetIdMap.keySet()) {
            Object lifeline = lifelineObjetIdMap.get(id);
            if (lifeline instanceof Bridge) {
                bridge = (Bridge)lifeline;
            }
        }
        return bridge;
    }

    private void verifyLifelines(Map<String,Object> lifelinesIdMap) throws InvalidModelException {
        Set<String> foundLifelineStereotypes = new HashSet<>();
        for (String key: lifelinesIdMap.keySet()) {
            VerifiableElement lifeline = (VerifiableElement) lifelinesIdMap.get(key);
            lifeline.checkForError();
            String className = lifeline.getClass().getSimpleName();
            if (className.equals(BRIDGE_STEREOTYPE_NAME) && foundLifelineStereotypes.contains(className)) {
                throw new InvalidModelException("Only one bridge is allowed per diagram.");
            }
            foundLifelineStereotypes.add(className);
        }
        //OCL constraint verification
        if (!foundLifelineStereotypes.contains(SMART_CONTRACT_STEREOTYPE_NAME)) {
            throw new InvalidModelException("There should be a SmartContract in the model.");
        }
        if (!foundLifelineStereotypes.contains(BRIDGE_STEREOTYPE_NAME)) {
            throw new InvalidModelException("There should be one Bridge in the model.");
        }
        if (!foundLifelineStereotypes.contains(OFF_CHAIN_APPLICATION_STEREOTYPE_NAME)) {
            throw new InvalidModelException("There should be an OffChainApplication in the model.");
        }
    }

    private Map<String, Lifeline> getLifelineElements(Interaction sequenceDiagram) {
        Map<String, Lifeline> lifelineElementsIdMap = new HashMap<>();
        for (Element element: sequenceDiagram.getOwnedElement()) {
            if (element.getClass().getSimpleName().equals(LIFELINE_CLASS_NAME)) {
                lifelineElementsIdMap.put(element.getID(), (Lifeline) element);
            }
        }
        return lifelineElementsIdMap;
    }

    /**
     * Iterates through lifeline elements of the sequence diagram and creates the lifeline Java objects
     * @param sequenceDiagram the sequence diagram
     * @param
     * @return lifelinesIdMap to associate each lifeline Java object with its id in the model
     * @throws InvalidModelException if there is more than one bridge, if there is no SmartContract
     * or if there is no OffChainApplication
     */
    private Map<String, Object> getLifelineObjects(Interaction sequenceDiagram) throws InvalidModelException {
        Map<String, Object> lifelinesIdMap = new HashMap<>();
        //for loop:
        //- create all lifeline Java objects (SmartContract, Bridge and OffChainApplication)
        for (Element element: sequenceDiagram.getOwnedElement()) {
            if (element.getClass().getSimpleName().equals(LIFELINE_CLASS_NAME)) {
                Stereotype stereotype = getElementStereotype(element);
                String lifelineStereotypeName = stereotype.getName();
                List<Tuple> lifelineAttributes = getTaggedValues(element, stereotype);
                Object lifeline = createClassObjectUsingReflection(lifelineStereotypeName, lifelineAttributes);
                lifelinesIdMap.put(element.getID(), lifeline);
            }
        }
        if (lifelinesIdMap.size() == 0) {
            throw new InvalidModelException("No lifeline object detected");
        }

        return lifelinesIdMap;
    }

    /**
     * Iterates through elements of a sequence diagram to find the model element that represents an off-chain event subscription
     * @param sequenceDiagram sequence diagram in which we want to find an off-chain event subscription
     * @return the CombinedFragment that represents an off-chain event subscription
     */
    private CombinedFragment getOffChainEventSubscriptionElement(Interaction sequenceDiagram) throws InvalidModelException {
        for (Element element: sequenceDiagram.getOwnedElement()) {
            if (element.getClass().getSimpleName().equals(COMBINED_FRAGMENT_CLASS_NAME)) {
                Stereotype stereotype = getElementStereotype(element);
                String lifelineStereotypeName = stereotype.getName();
                if (lifelineStereotypeName.equals(OFF_CHAIN_EVENT_SUBSCRIPTION_STEREOTYPE_NAME)) {
                    return (CombinedFragment)element;
                }
            }
        }
        return null;
    }


    /**
     * Creates the Java object representing the off-chain event subscription.
     * @param sequenceDiagram the diagram
     * @param orderedElements the ordered (and verified) messages
     * @return the generated Java object
     * @throws InvalidModelException
     */
    private OffChainEventSubscription getOffChainEventSubscription(Interaction sequenceDiagram, List<OracleInteraction> orderedElements) throws InvalidModelException {
        CombinedFragment offChainEventSubscriptionElement = getOffChainEventSubscriptionElement(sequenceDiagram);
        if (offChainEventSubscriptionElement == null) {
            throw new InvalidModelException("The model is missing an OffChainEventSubscription element.");
        }
        //query interval tagged value
        OffChainEventSubscription offChainEventsubcriptionObject = createOffChainEventSubscriptionWithReflection(offChainEventSubscriptionElement);
        if (offChainEventsubcriptionObject.getQueryInterval() <= 0) {
            throw new InvalidModelException("The query interval value should be greater than 0.");
        }
        //off-chain query tagged value
        OffChainQuery offChainQueryObject = (OffChainQuery) orderedElements.get(0);
        offChainEventsubcriptionObject.setOffChainQuery(offChainQueryObject);
        //opt: condition
        Object conditionElement = iterateUntilClassFound(offChainEventSubscriptionElement.getOperand().get(0).getFragment(), COMBINED_FRAGMENT_IMPL);
        if (conditionElement == null) {
            throw new InvalidModelException(OFF_CHAIN_EVENT_SUBSCRIPTION_STEREOTYPE_NAME + " should contain a trigger condition.");
        }
        try {
            CombinedFragment opt = (CombinedFragment)conditionElement ;
            String condition = ((LiteralString)opt.getOperand().get(0).getGuard().getSpecification()).getValue();
            OracleConstraint constraint = new OracleConstraint(condition);
            offChainEventsubcriptionObject.addConstraint(constraint); //TODO support more than one constraint if necessary
        } catch (Exception e) {
            throw new InvalidModelException("Invalid conditional expression for the off-chain event subscription");
        }
        return offChainEventsubcriptionObject;
    }

    private OffChainEventSubscription createOffChainEventSubscriptionWithReflection(Element element) throws InvalidModelException {
        Stereotype stereotype = getElementStereotype(element);
        String lifelineStereotypeName = stereotype.getName();
        List<Tuple> lifelineAttributes = getTaggedValues(element, stereotype);
        OffChainEventSubscription result = (OffChainEventSubscription)createClassObjectUsingReflection(lifelineStereotypeName, lifelineAttributes);
        return result;
    }

    /**
     * Goes through each transaction argument found in the model and create a TransactionArgument object.
     * Gets parameters from the ABI file in the correct order.
     * Adds each TransactionArgument to the SendTransaction in the specified order.
     * @param transaction element
     * @param transactionObject object
     * @throws InvalidModelException
     */
    private void parseSCTransactionArguments(Message transaction, SendTransaction transactionObject) throws InvalidModelException {
        if (transactionObject.getSmartContractFunctionName() == null) {
            return;
        }
        Map<String, TransactionArgument> transactionArguments = new HashMap<>();
        for (ValueSpecification vs:transaction.getArgument()) {
            TransactionArgument ta = new TransactionArgument();
            //TODO add verification: expression is ok, expression is opaque expression
            try {
                if (!vs.getClass().getSimpleName().equals(OPAQUE_EXPRESSION_CLASS_NAME)) {
                    throw new InvalidModelException("Arguments of smart contract transactions should be Opaque Expressions");
                }
                String argument = ((OpaqueExpression)vs).getBody().get(0);
                // query response argument means using a return value from the off-chain query
                // as a parameter in the transaction
                String SCTransactionArgumentName = argument.split("=")[0];
                String value;
                if (argument.contains("query")) {
                    ta.setQueryResponseArgument(true);
                    String [] split = argument.split("=");
                    value = split[1].split("\\.")[1]; // here the value is the name of the query parameter
                } else {
                    String [] split = argument.split("=");
                    value = split[1];
                }
                ta.setValue(value);
                transactionArguments.put(SCTransactionArgumentName, ta);
                ta.setSCTransactionArgumentName(SCTransactionArgumentName);
            } catch (Exception e) {
                throw new InvalidModelException("Invalid SendTransactionParameters");
            }

        }
        //add transaction arguments in the correct order
        List<String> orderedParameterNames = transactionObject.getOrderedParameterNames();
        for (String parameterName : orderedParameterNames) {
            TransactionArgument ta = transactionArguments.get(parameterName);
            transactionArguments.remove(parameterName);
            transactionObject.addArgument(ta);
        }
        if (transactionArguments.size() > 0) {
            String parameterName = transactionArguments.keySet().iterator().next();
            throw new InvalidModelException(String.format("Invalid argument for SendTransaction: %1$s", parameterName));
        }
    }

    /** Creates an object using the tagged values of the model.
     *  The attribute names in the model must match the attribute names of the object class.
     * @param className the name of the class of the object to be returned
     * @param attributes list of tuple (attribute name and value)
     * @return the object extracted from the model
     */
    private java.lang.Object createClassObjectUsingReflection(String className, List<Tuple> attributes) throws InvalidModelException {
        String classPath = this.getClass().getPackage().getName() + "." + className;
        try {
            Class c = Class.forName(classPath);
            java.lang.Object result = c.newInstance();
            for (Tuple t: attributes) {
                Field field = c.getDeclaredField(t.name);
                field.setAccessible(true);
                field.set(result, t.value);
            }
            return result;
        } catch (Exception e) {
            throw new InvalidModelException(e.toString());
        }
    }

    private List<Element> getElementsWithClassName(Iterator it, String className) {
        List<Element> result = new ArrayList<Element>();
        for (; it.hasNext();) {
            Element ownedElement = (Element) it.next();
            if (ownedElement.getClass().getSimpleName().equals(className)) {
                result.add(ownedElement);
            }
        }
        return result;
    }

    private Object iterateUntilClassFound(List<InteractionFragment> list, String className) {
        for (Object el: list) {
            if (el.getClass().getSimpleName().equals(className)) {
                return el;
            }
        }
        return null;
    }

    private Stereotype getElementStereotype(Element element) throws InvalidModelException {
        try {
            return (Stereotype) element.getAppliedStereotypeInstance().getClassifier().get(0);
        } catch (NullPointerException e) {
            throw new InvalidModelException("All elements in the diagram must have a valid stereotype");
        }
    }

    private List<Tuple> getTaggedValues(Element element, Stereotype stereotype) {
        List<Tuple> result = new ArrayList<>();
        List<Property> attributes = stereotype.getOwnedAttribute();
        for (Property p: attributes) {
            if (p.getName().startsWith("base_")) continue;
            String name = p.getName();
            List values = StereotypesHelper.getStereotypePropertyValue(element, stereotype, name);
            if (values.size() > 0) {
                result.add(new Tuple(name, values.get(0)));
            }
        }
        return result;
    }
}
