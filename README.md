# Model-Driven Engineering of Blockchain Oracles

## Repository files
* [demo](./demo): This directory contains files to show a short demonstration of MagicDraw plugin and the generated oracle application.
  * [thesis_demo.mp4](./demo/thesis_demo.mp4): A video that shows the oracle application in action. This oracle application supports the four
  oracles modeled in [lottery.mdzip](./demo/).
  * [offchain.js](./demo/offchain.js): The simple node.js application used in the demonstration video.
  * [lottery.mdzip](./demo/lottery.mdzip): The MagicDraw model for the four oracles that are used in the demonstration video.
  * [Lottery.sol](./demo/Lottery.sol): The Solidity file for the smart contract used in the demonstration video.
* [diagrams](./diagrams)
  * [abstract_syntax.mdzip](./node/abstract_syntax.mdzip): A UML class diagram representing the main concepts of the blockchain oracle domain.
  * [oracle_uml_profile.mdzip](./node/oracle_uml_profile.mdzip): The UML profile containing elements specific to the blockchain oracle domain.
    This file is needed to model oracles.
  * [use_cases.mdzip](./node/use_cases.mdzip): A UML use case diagram showing the use cases of the MagicDraw plugin and the generated oracle
  application.
* [node](./node)
  * [package.json](./node/package.json): The list of dependencies of the node.js oracle application.
* [plugin](./plugin)
  * [icons](./plugin/icons): The image files used by the plugin.
  * [templates](./plugin/templates): The template files used in the automatic code generation from the model. The MagicDraw plugin replaces the 
  placeholders in these files by the appropriate elements according to the model.
  * [buildplugin.ps1](./plugin/build/buildplugin.ps1): A Powershell script file to compile and package the plugin from the source files.
  * Java files: All the java classes needed for the plugin.

## MagicDraw

This section provides general information about using the MagicDraw plugin and modeling oracles.

### Open the UML profile of blockchain oracles

Before oracles can be modeled, the UML profile containing stereotypes and tagged values specific to the blockchain
oracle domain must be opened in MagicDraw.

* Open MagicDraw application.
* Select File -> Use Project -> Use Local Project -> From file system.
* Select the `.mdzip` file.

### Basic information for modeling oracles

For more information, consult the official [MagicDraw documentation](https://docs.nomagic.com/display/MD190/MagicDraw+Documentation).

#### Create a sequence diagram
* Click on Create Diagram and select Sequence Diagram.
* In the containment tree, make sure that the diagram is a child element of the doot <em>Model</em>, otherwise
  the plugin will not find the diagram.

#### Adding a lifeline and its stereotype
 * Select the Lifeline element from the left pane.
 * Right-click on the element and select Specification.
 * Click on Applied Stereotype and select the appropriate stereotype.
 * In the Tags section, add the necessary tagged values.

#### Adding a message and its stereotype
* The steps are the same as for the lifeline, except that the element added to the diagram should be a Message.
* Make sure that the message starts from the right Lifeline and ends at the right Lifeline.

#### Push-based inbound oracle specific details
The push-based inbound oracles has some elements that the other oracles do not have. These elements are the Loop and the Opt.

##### Loop
* Select the Loop element from the left pane.
* Add the OffChainEventSubscription stereotype to that element.
* Set the integer value of the queryInterval tagged value.


##### Opt
* Select the Option element from the left pane.
* The Opt should contain a condition to trigger the transaction inside of it. 
    * Double-click on the Opt element.
    * Select Operands.
    * Click on Create.
    * Click on Guard.
    * Write the condition. The left side should be the name of a variable inside the response to the OffChainQuery.
      The operator should be !=, ==, >, >=, <, or <=. The right side of the condition should be a string or an integer.
* Make sure the Opt element is inside the Loop element. Resize the Loop if needed.

#### Additional tips and information

* Use only sequence diagrams
* In the `demo` directory, the [lottery.mdzip](./demo/lottery.mdzip) file contains an example for the four types of oracles.
* Make sure that all required elements are present and that they are in the correct order.

### Generate the source code
* Click on the icon with a green circle inside of it, or press Ctrl + Shift + E.

### Making modifications to the plugin

Everytime some modifications are made to the plugin, i.e. everytime the Java classes in [plugin](./plugin) are modified, 
the classes must be recompiled and repackaged. The resulting `.jar` file must be placed in the `plugins` directory of the MagicDraw
installation directory. Additionally, the `.jar` file must contain a `plugin.xml` file containing general information about the plugin
so that MagicDraw recognized the `.jar` file as a MagicDraw plugin. The [buildplugin.ps1](./plugin/build/buildplugin.ps1) file shows an 
example of how this can be done on a Windows environment. For more information, see the official MagicDraw [documentation](https://docs.nomagic.com/display/MD185/Plugins).
It is worth noting that a common cause of failure when trying to load a plugin is that the namespace in the Java files (e.g. `package oracleplugin;`) 
is not consistent with the structure of the file packaged in the `.jar` file or the `class` attribute of the `plugin.xml`.

## Generated oracle application

The generated oracle application is a Node.js application. You need to install [node](https://nodejs.org/en/)
before you can use it.

### Dependencies

The dependencies are listed in `node/package.json`.

### TLS certificates generation

To authenticate the clients making requests, the oracle needs to have access to a certificate
authority certificate. Using the certificate authority certificate (CA certificate), the oracle
can verify that the certificate provided by the client is signed by the CA certificate.
Additionally, the oracle needs access to a key pair that will be used for encrypting
and ensuring the integrity of the communication with other entities.
Finally, the client might want to authenticate the oracle too, so the oracle needs a
certificate signed by a certificate authority that is trusted by the clients.


#### CA certificate

The CA certificate generated by the following command is a self-signed certificate
valid for 365 days. The nodes parameter can be removed to protect the private with
a password. Running this command will prompt the user for information about the
certificate, like the organization name and the certificate’s common name.

`openssl req -x509 -nodes -newkey rsa:4096 -keyout ca_key.pem -out ca_certificate.pem -days 365`

#### The oracle's certificate and keys

The oracle's certificate and keys are generated using the 3 commands shown below.
First a keypair must be generated before a certificate can be created. The following
command creates an RSA private key of a size of 4096 bits.

`openssl genrsa -out oracle_key.pem 4096`

Then, a certificate signing request (CSR) must be created using the command below.
As for the CA certificate, this command will prompt the user for information about the
certificate and the challenge password is optional. Make sure that the common name is
the named of the host where the oracle will run. For example, if running the oracle on a
local computer, then the common name should be localhost.

`openssl req -new -sha256 -key oracle_key.pem -out oracle_csr.pem`

Finally, the CSR must be signed by the CA’s private key to create the certificate using
the following command.

`openssl x509 -req -days 365 -in oracle_csr.pem -CA ca_certificate.pem -CAkey ca_key.pem -CAcreateserial -out oracle_certificate.pem`

To add alternative subject names to the certificate, the following option can be added
to the previous command:

`-extfile <(printf"subjectAltName=DNS:DESKTOP-FQK0S45.local")`

### How to run
* Make sure that you have generated the `oracle.js` file from the plugin.
* Run `npm install` from the directory containing the `package.json` file in order to install the dependencies.
* Run `node ./oracle.js` to execute the oracle application.

## Notes

* The oracle application and the MagicDraw plugin have only been tested on a Windows 10 machine.